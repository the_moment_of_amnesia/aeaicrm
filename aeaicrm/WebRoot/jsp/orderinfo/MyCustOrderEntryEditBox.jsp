<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if (responseText == 'success'){
			hideSplash();
			parent.PopupBox.closeCurrent();
			parent.refreshPage();
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("doEdit8Save")){ %>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td></aeai:previlege>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
   <%} %>
   <aeai:previlege code="close"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>预定产品</th>
	<td><input id="ENTRY_ORDER_PRODUCT" label="预定产品" name="ENTRY_ORDER_PRODUCT" type="text" value="<%=pageBean.inputValue("ENTRY_ORDER_PRODUCT")%>" size="24" style="width: 302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>产品型号</th>
	<td><input id="ENTRY_PRODUCT_MODEL" label="产品型号" name="ENTRY_PRODUCT_MODEL" type="text" value="<%=pageBean.inputValue("ENTRY_PRODUCT_MODEL")%>" size="24" style="width: 302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>数量</th>
	<td><input id="ENTRY_NUMBER" label="数量" name="ENTRY_NUMBER" type="text" maxlength="6" value="<%=pageBean.inputValue("ENTRY_NUMBER")%>" size="24" style="width: 302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>单价</th>
	<td><input id="ENTRY_UNIT_PRICE" label="单价" name="ENTRY_UNIT_PRICE" type="text" maxlength="8" value="<%=pageBean.inputValue("ENTRY_UNIT_PRICE")%>" size="24" style="width: 302px" class="text" />
(单位：万)</td>
</tr>
<tr>
	<th width="100" nowrap>折扣</th>
	<td><input id="ENTRY_DISCOUNT" label="折扣" name="ENTRY_DISCOUNT" type="text" maxlength="6" value="<%=pageBean.inputValue("ENTRY_DISCOUNT")%>" size="24" style="width: 302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>实际价格</th>
	<td><input id="ENTRY_REAL_PRICE" label="实际价格" name="ENTRY_REAL_PRICE" type="text" value="<%=pageBean.inputValue("ENTRY_REAL_PRICE")%>" readonly="readonly" size="24" style="width: 302px" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ENTRY_ID" name="ENTRY_ID" value="<%=pageBean.inputValue("ENTRY_ID")%>" />
<input type="hidden" id="ORDER_ID" name="ORDER_ID" value="<%=pageBean.inputValue("ORDER_ID")%>" />
</form>
<script language="javascript">
requiredValidator.add("ENTRY_ORDER_PRODUCT");
requiredValidator.add("ENTRY_PRODUCT_MODEL");
numValidator.add("ENTRY_NUMBER");
numValidator.add("ENTRY_UNIT_PRICE");
numValidator.add("ENTRY_DISCOUNT");
numValidator.add("ENTRY_REAL_PRICE");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
