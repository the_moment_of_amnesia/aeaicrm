package com.agileai.crm.cxmodule;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface ClueInfoManage
        extends StandardService {

	void createCustomerRecord(DataParam param);
	void createOppRecord(DataParam param);
	void assignRecord(DataParam param);
	void doClaimRecord(String clueId);
	void doDisposeRecord(DataParam param);
}
